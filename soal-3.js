const checkEmail = (email) => {
    let regexEmail = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/; // Format Regex Email

    // Validasi jika parameter Null
    if (email == null) return "ERROR : Where is the parameters?"

    // Validasi tipe data
    if (typeof email != "string") return "ERROR : INVALID data Type! It must be 'String'"

    // Validasi bila tidak terdapat simbol '@' pada email
    if (!email.match("@")) return "ERROR : Email Salah! Silahkan masukan Email sesuai dengan format berikut ==> 'rizkidelaga@gmail.com'"

    // Pengecekan email sesuai dengan format regexEmail
    if (email.match(regexEmail)) return "VALID"
    
    return "INVALID";
};

// -------------------------------------------------------------------------------
// Output Code
// -------------------------------------------------------------------------------

console.log("=========================== No 3 ===========================");

console.log("\n----------------------- Hasil Benar! -----------------------");
console.log(checkEmail("rizki@gmail.com"));
console.log(checkEmail("rizki@gmail.co.id"));

console.log("\n----------------------- Hasil Salah! -----------------------");
console.log(checkEmail("rizki@gmail."));
console.log(checkEmail("rizki@"));

console.log("\n------------------ (Penanganan Kesalahan) ------------------");
console.log(checkEmail("rizki"));
console.log(checkEmail(67421));
console.log(checkEmail());

console.log("\n============================================================");