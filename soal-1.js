const changeWord = (selectedText, changedText, text) => {

    // Validasi parameter
    if (text == null || selectedText == null || changedText == null) return "ERROR : Where is the parameters?"

    let arrayKata = text.split(" "); // Split kalimat per kata kedalam array

    // Ganti kata yang sama dengan variabel changedText
    for (let c = 0; c < arrayKata.length; c++) {
        if (arrayKata[c] == selectedText) arrayKata[c] = changedText
    }
    let resultChangeWord = arrayKata.join(" "); // Gabungkan array kedalam variabel

    // Validasi perubahan
    // Jika tidak ada kata yang diubah, tampilkan output dibawah ini 
    if (resultChangeWord == text) resultChangeWord = `Tidak ada kata "${selectedText}" didalam kalimat "${text}"`

    return resultChangeWord;
}

// -------------------------------------------------------------------------------
// Output Code
// -------------------------------------------------------------------------------

const kalimat1 = "Andini sangat mencintai kamu selamanya"
const kalimat2 = "Gunung bromo tak akan mampu menggambarkan besarnya cintaku padamu"

console.log("=========================== No 1 ===========================");

console.log("\n----------------------- Hasil Benar! -----------------------");
console.log(changeWord("mencintai", "membenci", kalimat1));
console.log(changeWord("bromo", "semeru", kalimat2));
console.log(changeWord("tiga", "tujuh", "Buku itu sudah saya baca tiga kali"));

console.log("\n----------------------- Hasil Salah! -----------------------");
console.log(changeWord("merapi", "semeru", kalimat2));
console.log(changeWord(11322, "semeru", kalimat2));
console.log(changeWord("merapi", 123312, kalimat2));

console.log("\n------------------ (Penanganan Kesalahan) ------------------");
console.log(changeWord());

console.log("\n============================================================");