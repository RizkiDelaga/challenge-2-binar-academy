const getSplitName = (personName) => {
    const namaOrang = {
        firstName: null,
        middleName: null,
        lastName: null
    }

    // Validasi jika parameter Null
    if (personName == null) return "ERROR : Where is the parameters?"

    // Validasi tipe data
    if (typeof personName != "string") return "ERROR : INVALID data Type! It must be 'String'"

    // Split Name by ==> " "
    const splitName = personName.split(" ");

    // Validasi panjang nama
    if (splitName.length > 3) return "ERROR : This function is only for 3 characters name"

    // Jika nama hanya terdiri dari 2 kata
    if (splitName.length == 2) {
        // Masukan Nama by Index ke dalam Object
        namaOrang.firstName = splitName[0] || null
        namaOrang.middleName = null
        namaOrang.lastName = splitName[1] || null
    } else {
        // Masukan Nama by Index ke dalam Object
        namaOrang.firstName = splitName[0] || null
        namaOrang.middleName = splitName[1] || null
        namaOrang.lastName = splitName[2] || null
    }

    return namaOrang;
}

// -------------------------------------------------------------------------------
// Output Code
// -------------------------------------------------------------------------------

console.log("=========================== No 5 ===========================");

console.log("\n----------------------- Hasil Benar! -----------------------");
console.log(getSplitName("Rizki Delaga Prasetya"));
console.log(getSplitName("Rizki Delaga"));
console.log(getSplitName("Rizki"));

console.log("\n------------------ (Penanganan Kesalahan) ------------------");
console.log(getSplitName("Rizki Delaga Prasetya Hallo"));
console.log(getSplitName(0));
console.log(getSplitName());

console.log("\n============================================================");