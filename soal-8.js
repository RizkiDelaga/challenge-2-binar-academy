const getInfoPenjualan = (dataPenjualan) => {
    // Inisialisasi Object
    let result = {
        totalKeuntungan: "",
        totalModal: "",
        persentaseKeuntungan: "",
        produkBukuTerlaris: "",
        penulisTerlaris: ""
    }

    // Inisialisasi variabel untuk menampung data
    let tempArray = []
    let tempData = 0

    if (dataPenjualan == null) { // Validasi jika parameter Null
        return "ERROR : Where is the parameters?"
    } else if (typeof dataPenjualan != "object") { // Validasi tipe data
        return "ERROR : INVALID data Type! It must be 'Object'"
    } else {
        // Perhitungan totalKeuntungan
        for (let i = 0; i < dataPenjualan.length; i++) {
            tempArray[i] = (dataPenjualan[i].hargaJual - dataPenjualan[i].hargaBeli) * dataPenjualan[i].totalTerjual
            tempData += tempArray[i]
        }
        result.totalKeuntungan = tempData.toString()
        // Kosongkan variabel
        tempArray = []
        tempData = 0

        // Perhitungan totalModal
        for (let i = 0; i < dataPenjualan.length; i++) {
            tempArray[i] = dataPenjualan[i].hargaBeli * (dataPenjualan[i].totalTerjual + dataPenjualan[i].sisaStok)
            tempData += tempArray[i]
        }
        result.totalModal = tempData.toString()
        // Kosongkan variabel
        tempArray = []
        tempData = 0

        // Masukan data-data dari totalTerjual ke dalam array untuk digunakan buat proses Sorting
        for (let i = 0; i < dataPenjualan.length; i++) {
            tempArray[i] = dataPenjualan[i].totalTerjual
        }

        tempArray.sort((a, b) => a - b) // Sort array
        // Perhitungan penulisTerlaris dan produkBukuTerlaris
        for (let i = 0; i < tempArray.length; i++) {
            if (dataPenjualan[i].totalTerjual == tempArray[tempArray.length - 1]) {
                result.penulisTerlaris = dataPenjualan[i].penulis
                result.produkBukuTerlaris = dataPenjualan[i].namaProduk
            }
        }
        // Kosongkan variabel
        tempArray = []
        tempData = 0

        // Perhitungan persentaseKeuntungan
        tempData = (Number(result.totalKeuntungan) / Number(result.totalModal)) * (100)
        result.persentaseKeuntungan = tempData.toFixed(2) + "%"

        // Ubah totalKeuntungan kedalam format Rupiah
        result.totalKeuntungan = "Rp. " + result.totalKeuntungan.split("").reverse().join("").match(/\d{1,3}/g).join(".").split("").reverse().join("")
        
        // Ubah totalModal kedalam format Rupiah
        result.totalModal = "Rp. " + result.totalModal.split("").reverse().join("").match(/\d{1,3}/g).join(".").split("").reverse().join("")
    }

    return result
}

// -------------------------------------------------------------------------------
// Output Code
// -------------------------------------------------------------------------------

const dataPenjualanNovel = [{
        idProduct: 'BOOK002421',
        namaProduk: 'Pulang - Pergi',
        penulis: 'Tere Liye',
        hargaBeli: 60000,
        hargaJual: 86000,
        totalTerjual: 150,
        sisaStok: 17,
    },
    {
        idProduct: 'BOOK002351',
        namaProduk: 'Selamat Tinggal',
        penulis: 'Tere Liye',
        hargaBeli: 75000,
        hargaJual: 103000,
        totalTerjual: 171,
        sisaStok: 20,
    },
    {
        idProduct: 'BOOK002941',
        namaProduk: 'Garis Waktu',
        penulis: 'Fiersa Besari',
        hargaBeli: 67000,
        hargaJual: 99000,
        totalTerjual: 213,
        sisaStok: 5,
    },
    {
        idProduct: 'BOOK002941',
        namaProduk: 'Laskar Pelangi',
        penulis: 'Andrea Hirata',
        hargaBeli: 55000,
        hargaJual: 68000,
        totalTerjual: 20,
        sisaStok: 56,
    },
];

console.log("=========================== No 8 ===========================")

console.log("\n----------------------- Hasil Benar! -----------------------")
console.log(getInfoPenjualan(dataPenjualanNovel))

console.log("\n------------------ (Penanganan Kesalahan) ------------------")
console.log(getInfoPenjualan("dataPenjualanNovel"))
console.log(getInfoPenjualan(1231241))
console.log(getInfoPenjualan())

console.log("\n============================================================")