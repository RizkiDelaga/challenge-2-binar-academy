const checkTypeNumber = (givenNumber) => {

    // Validasi jika parameter Null
    if (givenNumber == null) return "Error : Bro where is the parameter?"

    // Tipe data salah
    if (typeof givenNumber != "number") return "Error : Invalid data Type"

    // Pengecekan bilangan GANJIL/GENAP
    if (givenNumber % 2 == 0) return "GENAP"

    return "GANJIL";
}

// -------------------------------------------------------------------------------
// Output Code
// -------------------------------------------------------------------------------

console.log("=========================== No 2 ===========================");

console.log("\n----------------------- Hasil Benar! -----------------------");
console.log(checkTypeNumber(12));
console.log(checkTypeNumber(3));

console.log("\n------------------ (Penanganan Kesalahan) ------------------");
console.log(checkTypeNumber("21"));
console.log(checkTypeNumber({}));
console.log(checkTypeNumber());

console.log("\n============================================================");