const isValidPassword = (givenPassword) => {
    let regexPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/; // Format Regex Password

    // Validasi jika parameter Null
    if (givenPassword == null) return "ERROR : Where is the parameters?"
    
    // Validasi tipe data
    if (typeof givenPassword != "string") return "ERROR : Password harus berupa 'String' yang mengandung Huruf Besar, Huruf Kecil, Angka dan Minimal 8 Karakter!"
    
    // Pengecekan password sesuai dengan format regexPassword
    if (givenPassword.match(regexPassword)) return true
    
    return false;
}

// -------------------------------------------------------------------------------
// Output Code
// -------------------------------------------------------------------------------

console.log("=========================== No 4 ===========================");

console.log("\n----------------------- Hasil Benar! -----------------------");
console.log(isValidPassword("RsdjeLr3"));

console.log("\n----------------------- Hasil Salah! -----------------------");
console.log(isValidPassword("asdjeiru"));
console.log(isValidPassword("@asd"));

console.log("\n------------------ (Penanganan Kesalahan) ------------------");
console.log(isValidPassword());
console.log(isValidPassword(12372159));

console.log("\n============================================================");