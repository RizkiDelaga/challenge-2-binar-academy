const hitungTotalPenjualan = (dataPenjualan) => {

    // Validasi jika parameter Null
    if (dataPenjualan == null) return "ERROR : Where is the parameters?"

    // Validasi tipe data
    if (typeof dataPenjualan != "object") return "ERROR : INVALID data Type! It must be 'Object'"

    // Hitung Total Penjualan
    let sumDataPenjualan = 0;
    for (let i = 0; i < dataPenjualan.length; i++) {
        sumDataPenjualan += dataPenjualan[i].totalTerjual // Nilai dari totalTerjual ditambahkan ke dalam variabel sumDataPenjualan
    }

    return sumDataPenjualan;
}

// -------------------------------------------------------------------------------
// Output Code
// -------------------------------------------------------------------------------

const dataPenjualanPakAldi = [{
        namaProduct: 'Sepatu Futsal Nike Vapor Academy 8',
        hargaSatuan: 760000,
        kategori: "Sepatu Sport",
        totalTerjual: 90,
    },
    {
        namaProduct: 'Sepatu Warrior Tristan Black Brown High',
        hargaSatuan: 960000,
        kategori: "Sepatu Sneaker",
        totalTerjual: 37,
    },
    {
        namaProduct: 'Sepatu Warrior Tristan Maroon High ',
        kategori: "Sepatu Sneaker",
        hargaSatuan: 360000,
        totalTerjual: 90,
    },
    {
        namaProduct: 'Sepatu Warrior Rainbow Tosca Corduroy',
        hargaSatuan: 120000,
        kategori: "Sepatu Sneaker",
        totalTerjual: 90,
    }
]

console.log("=========================== No 7 ===========================");

console.log("\n----------------------- Hasil Benar! -----------------------");
console.log(hitungTotalPenjualan(dataPenjualanPakAldi));

console.log("\n------------------ (Penanganan Kesalahan) ------------------");

console.log(hitungTotalPenjualan("dataPenjualanPakAldi"));
console.log(hitungTotalPenjualan(1237732189));
console.log(hitungTotalPenjualan());

console.log("\n============================================================");