const getAngkaTerbesarKedua = (dataNumbers) => {
    // Validasi jika parameter Null
    if (dataNumbers == null) return "ERROR : Where is the parameters?"

    // Validasi tipe data
    if (typeof dataNumbers != "object") return "ERROR : Array Tidak Ditemukan! Silahkan masukan Array yang menampung sebuah angka"

    // Sort angka di daman array dari yang terkecil-terbesar
    const sortNumber = dataNumbers.sort((a, b) => a - b);

    let resultAngka;
    // Pengecekan angka tiap Index mulai dari yang paling belakang
    for (let i = dataNumbers.length; i > 0; i--) {
        // Jika angka pada Index tidak sama
        if (sortNumber[i - 2] != sortNumber[i - 1]) return resultAngka = sortNumber[i - 2]
    }
}

// -------------------------------------------------------------------------------
// Output Code
// -------------------------------------------------------------------------------

console.log("=========================== No 6 ===========================");

const dataAngka1 = [3, 7, 6, 1, 4, 7, 7, 5, 3];
const dataAngka2 = ["3", "7", 73, 6, "1", "52", "43", 7, 110, "7", "5", 3];

console.log("\n----------------------- Hasil Benar! -----------------------");
console.log(getAngkaTerbesarKedua(dataAngka1));
console.log(getAngkaTerbesarKedua(dataAngka2));

console.log("\n------------------ (Penanganan Kesalahan) ------------------");
console.log(getAngkaTerbesarKedua(0));
console.log(getAngkaTerbesarKedua());

console.log("\n============================================================");